const fs = require('fs');
const path = require('path');

/**
 * @description Read files synchronously from a folder, with natural sorting
 * @param {String} dir Absolute path to directory
 * @returns {Object[]} List of object, each object represent a file
 *
 */
function readFilesSync(dir) {
  const files = [];

  fs.readdirSync(dir).forEach(filename => {

    const file = path.parse(filename);
    const filepath = path.join(dir, filename);
    const stat = fs.statSync(filepath);
    const isFile = stat.isFile();

    if (isFile && file.ext === '.json')
      files.push(JSON.parse(fs.readFileSync(filepath)));
  });

  /* files.sort((a, b) => {
    // natural sort alphanumeric strings
    // https://stackoverflow.com/a/38641281
    return a.name.localeCompare(b.name, undefined, { numeric: true, sensitivity: 'base' });
  }); */

  return files;
}

module.exports = {
  places: readFilesSync('src/_data/')
}
